# ksurfshark.sh - Surfshark WireGuard VPN client.

## Running
Run these commands:
```sh
git clone https://gitlab.com/kernal/ksurfshark.git
cd ksurfshark
# Add your credentials.
vim ksurfshark.sh
./ksurfshark.sh
```
Now you should have all WireGuard configuration files at data/wireguard_configs
