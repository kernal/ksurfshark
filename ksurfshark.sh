#!/usr/bin/env sh

# ksurfshark.sh - Surfshark WireGuard VPN client.
# Copyright (C) 2022  Laurynas Četyrkinas

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

surfshark_username='surfshark@example.net'
surfshark_password='Sup3rSecure'
work_dir=data

surfshark_baseurl='https://api.surfshark.com'
surfshark_token_file="$work_dir/surfshark_token.txt"
surfshark_servers_file="$work_dir/surfshark_servers.json"
wireguard_privatekey_file="$work_dir/wireguard_privatekey.txt"
wireguard_dir="$work_dir/wireguard_configs"

mkdir -p "$work_dir"

create_token() {
	echo 'Creating a token...'
	curl -# "$surfshark_baseurl/v1/auth/login" \
		-H 'Content-Type: application/json' \
		-d "{\"username\":\"$surfshark_username\",\"password\":\"$surfshark_password\"}" \
		| jq -r '.token' > "$surfshark_token_file"
}
if [ -s "$surfshark_token_file" ]; then
	create=`cat "$surfshark_token_file" | cut -d . -f 2 | base64 -d | jq '.exp < now - 10'`
	[ "$create" = "true" ] && create_token
else
	create_token
fi
surfshark_token=`cat "$surfshark_token_file"`

echo 'Fetching a list of servers...'
curl -# "$surfshark_baseurl/v4/server/clusters/all" -o "$surfshark_servers_file"

if [ -s "$wireguard_privatekey_file" ]; then
	wireguard_privatekey=`cat "$wireguard_privatekey_file"`
else
	wireguard_privatekey=`wg genkey | tee "$wireguard_privatekey_file"`
fi
wireguard_publickey=`echo "$wireguard_privatekey" | wg pubkey`

echo 'Submitting WireGuard public key...'
curl -# "$surfshark_baseurl/v1/account/users/public-keys" \
	-H 'Content-Type: application/json' \
	-H "Authorization: Bearer $surfshark_token" \
	-d "{\"pubKey\":\"$wireguard_publickey\"}" > /dev/null

echo 'Generating WireGuard configuration files...'
rm -rf "$wireguard_dir"
mkdir -p "$wireguard_dir"
jq '.[] | select(.pubKey != null) | .connectionName, .pubKey' "$surfshark_servers_file" \
	| xargs -n2 ./ksurfshark-helper.sh "$wireguard_privatekey" "$wireguard_dir"
# TODO: Move everything to a single script.
