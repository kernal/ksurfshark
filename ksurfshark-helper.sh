#!/usr/bin/env sh

# ./ksurfshark-helper.sh "$privatekey" "$directory" "$endpoint" "$publickey"
file="$2/ss_`echo "$3" | cut -d . -f 1`.conf"
cat <<EOF > "$file"
[Interface]
Address=10.14.0.2/16
PrivateKey=$1

[Peer]
PublicKey=o07k/2dsaQkLLSR0dCI/FUd3FLik/F/HBBcOGUkNQGo=
Endpoint=wgs.prod.surfshark.com:51820
PersistentKeepalive=25

[Peer]
PublicKey=$4
Endpoint=$3:51820
PersistentKeepalive=25
AllowedIPs=0.0.0.0/0
EOF
